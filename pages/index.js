import asset from "@/plugins/assets/asset"
import MasterPage from "src/master/MasterPage"

export default function Home() {
  return (
    <MasterPage>
      <div className="profile">
        <div className="avt">
          <img src={asset("/images/avt.jpg")} alt="" />
        </div>
        <div className="info">
          Nguyen An - Frontend Dev
        </div>
      </div>
    </MasterPage>
  )
}
