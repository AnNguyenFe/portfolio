import asset from "@/plugins/assets/asset";

const MobileStyle = () => (
  <style jsx global>{`
    @mixin rowMachine($numPerRow, $margin) {
      width: ((100% - (($numPerRow - 1) * $margin)) / $numPerRow);
      &:nth-child(n) {
        margin-bottom: $margin;
        margin-right: $margin;
      }
      &:nth-child(#{$numPerRow}n) {
        margin-right: 0;
        margin-bottom: 0;
      }
    }

    
    @media screen and (max-width: 1440px) {
        
    }

    @media screen and (max-width: 1180px) {
        
    }

    @media screen and (max-width: 850px) and (orientation: portrait) {

    }
    
    @media screen and (max-width: 850px) and (orientation: landscape) {
        
    }
    
    @media screen and (max-width: 767px) and (orientation: portrait) {
       
    }

    @media screen and (max-width: 480px) {
     
    }
  `}</style>
);

export default MobileStyle;
