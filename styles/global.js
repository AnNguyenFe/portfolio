// import "antd/dist/antd.min.css";

const GlobalStyle = () => (
  <style jsx global>{`
    /* 
		font-family: 'Cabin', sans-serif;
		font-family: 'Lora', serif;
		font-family: 'Playfair Display', serif;
		*/

    html,
    body {
      padding: 0;
      margin: 0;
      font-family: "Poppins", sans-serif;
      line-height: 1.3;
      font-size: 16px;
      width: 100%;
      color: #000;
      background: none;
  //    scroll-behavior: smooth;
  //scroll-padding: 70px;
      
    }

    #__next {
      // overflow-x: hidden;
      // width: 100%;
    }

    * {
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      -ms-box-sizing: border-box;
      box-sizing: border-box;
    }
    h1,
    h2,
    h3,
    h4,
    h5 {
      padding: 0;
      margin: 0;
      font-weight: bold;
    }

    p {
      padding: 0;
      margin: 0;
    }

    ul,
    li {
      padding: 0;
      margin: 0;
      list-style-type: none;
      padding-inline-start: 0px;
      margin-block-start: 0;
      margin-block-end: 0;
      line-height: 1;
    }

    ol li,
    ul li {
      line-height: 1.5em;
    }

    hr {
      border-top: 1px solid #dadada;
    }

    a,
    a:hover,
    a:active {
      text-decoration: none;
    }

    img {
      width: 100%;
      display: block;
    }

    select {
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;
      border: 0;
      background: none;
    }

    main {
    }

    pre {
      margin-bottom: 0;
    }
    code.hljs {
      padding: 0.8rem;
    }
    .desktop{
      overflow: hidden;
    }
   
  `}</style>
);

export default GlobalStyle;
