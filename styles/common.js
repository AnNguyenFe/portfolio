const CommonStyle = () => (
  <style jsx global>{`
    .profile {
      display: flex;
      justify-content: center;
      align-items: center;
      height: 100vh;
      font-size: 50px;
    }
    .avt {
      width: 200px;
      border-radius: 50%;
      border: 1px solid #000;
      overflow: hidden;
      margin-right: 20px;
    }
  `}</style>
);

export default CommonStyle;
